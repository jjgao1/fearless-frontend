import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendeeForm from "./AttendeeForm";
import PresentationForm from "./PresentationForm";
import { BrowserRouter as Router, Route, NavLink, BrowserRouter, Routes } from 'react-router-dom'
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route index element={<MainPage/>}/>
            <Route path="location">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="attendees">
              <Route path="new" element={<AttendeeForm />} />
            </Route>
            <Route path="conference">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="presentations">
              <Route path="new" element={<PresentationForm />} />
            </Route>
            <Route path="AttendeesList">
              <Route path="new" element={<props.attendees />} />
            </Route>


          </Routes>
          {/* <AttendeeForm /> */}
          {/* <ConferenceForm /> */}
          {/* <LocationForm /> */}
          {/* <PresentationForm /> */}
          {/* <AttendeesList attendees={props.attendees} /> */}
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
